{ config, lib, pkgs, inputs, ... }:
let
    home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/master.tar.gz";
in
{
    imports = [ 
        ../modules/default.nix
        ../pkgs/default.nix
        inputs.home-manager.nixosModules.default
    ];

    nix.settings.experimental-features = [ "nix-command" "flakes" ];

    boot.loader = {
        systemd-boot.enable = true;
        efi.canTouchEfiVariables = true;
    };

    hardware.graphics.enable = true;

    system.stateVersion = "23.05";
}
