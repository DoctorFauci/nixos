{ pkgs, inputs, outputs, ... }: {
    fonts.packages = with pkgs; [ nerd-fonts.jetbrains-mono ];

    programs.adb.enable = true;

    nixpkgs = {
        overlays = [
            outputs.overlays.modifications
        ];
    };

    environment = {
        interactiveShellInit = ''
            alias rebuild='sudo nixos-rebuild switch --flake ./#nixos'
            alias clean='sudo nix-collect-garbage -d'
            alias flakes='sudo nix flake update'
            alias channel='sudo nix-channel --update'
            alias yt='mpv --ytdl-format="bestvideo[height<=?720][fps<=?30][vcodec!=?vp9]+bestaudio/best"'
            alias music='mpv --ytdl-raw-options=proxy=[socks5://127.0.0.1:9050] --ytdl-format=bestaudio'
            alias shut='sudo shutdown -h now'
            alias reb='sudo reboot'
        '';
        systemPackages = with pkgs; [
            eclipses.eclipse-java
            neovim
            ripgrep
            palemoon-bin
            ungoogled-chromium
            networkmanager
            jmtpfs
            zathura
            nsxiv
            mpv
            bc
            unzip
            p7zip
            pamixer
            htop-vim
            git
            axel
            yt-dlp
            proxychains-ng
            xd
            st
            dmenu
            inputs.dwmstatus.packages.${system}.dwmstatus
            xwallpaper
            picom
        ];
    };
}
