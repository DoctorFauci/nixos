{
    description = "Nixos config flake";

    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
        dwmstatus.url = "gitlab:DoctorFauci/dwmstatus";
        home-manager = {
            url = "github:nix-community/home-manager";
            inputs.nixpkgs.follows = "nixpkgs";
        };
    };

    outputs = { 
        self,
        nixpkgs,
        ... 
        } @ inputs: let
            inherit (self) outputs;
            systems = [
                "aarch64-linux"
                "i686-linux"
                "x86_64-linux"
                "aarch64-darwin"
                "x86_64-darwin"
            ];
            forAllSystems = nixpkgs.lib.genAttrs systems;
        in {
        overlays = import ./overlays {inherit inputs;};
        nixosConfigurations = {
            nixos = nixpkgs.lib.nixosSystem {
                specialArgs = {inherit inputs outputs;};
                modules = [
                    ./nixos/configuration.nix
                    inputs.home-manager.nixosModules.default
                ];
            };
        };
    };
}
