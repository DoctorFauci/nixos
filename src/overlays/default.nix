# This file defines overlays
{inputs, ...}: {
    modifications = final: prev: {
        dwm = prev.dwm.overrideAttrs (oldAttrs: rec {
            src = fetchTarball {
                url = "https://gitlab.com/DoctorFauci/dwm-dracula/-/archive/master/dwm-dracula-master.tar.gz";
                sha256 = "sha256:0an1z5p3bnnrlbvb3v7nvzra20fyi4px28yf7wsbjlhx4lzyn1wc";
            };
        });
        st = prev.st.overrideAttrs (oldAttrs: rec {
            buildInputs = oldAttrs.buildInputs ++ [ prev.harfbuzz ];
            src = fetchTarball {
                url = "https://gitlab.com/DoctorFauci/st-dracula/-/archive/master/st-dracula-master.tar.gz";
                sha256 = "sha256:0kqcypp9pj6ysfq8y91gxhwwxa5lm0ikwzmgm4g4cgwm6b7b97ak";
            };
        });
        dmenu = prev.dmenu.overrideAttrs (oldAttrs: rec {
            src = fetchTarball {
                url = "https://gitlab.com/DoctorFauci/dmenu-dracula/-/archive/master/dmenu-dracula-master.tar.gz";
                sha256 = "sha256:124pwn4ii72916chbxi1wyfga872wg46nkgsfa8rzq8vwg7h7cvf";
            };
        });
    };
}
