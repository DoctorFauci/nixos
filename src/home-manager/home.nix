{ config, pkgs, ... }: {
    home.username = "anon";
    home.homeDirectory = "/home/anon";

    home.stateVersion = "24.05";

    home.file = {
        ".profile".source = pkgs.fetchurl {
            url ="https://gitlab.com/DoctorFauci/dotfiles/-/raw/master/.profile?ref_type=heads";
            sha256 = "sha256:yCI3l0GYZIBURs4CLcekCkwqZPsTdH84a3by+Okd8zk=";
        };
        ".xinitrc".source = pkgs.fetchurl {
            url = "https://gitlab.com/DoctorFauci/dotfiles/-/raw/master/.xinitrc?ref_type=heads";
            sha256 = "sha256:b1/aa0hDkjPDn4kw9Cg2735xXgift+1UUnJLKlnbBho=";
        };
        #".wallp/levy.jpg";
    };

    home.sessionPath = [
        "$HOME/.local/bin"
    ];

    programs = {
        home-manager.enable = true;
        git = {
            userName = "Doctor Fauci";
            userEmail = "doctorfauci@horsefucker.org";
        };
    };
}
