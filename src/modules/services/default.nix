{ ... }: {
    imports = [
        ./xserver.nix
        ./pipewire.nix
        ./tor.nix
        ./i2pd.nix
    ];
}
