{ pkgs, outputs, ... }: {
    nixpkgs = {
        overlays = [
            outputs.overlays.modifications
        ];
    };
    services.xserver = {
        enable = true;
        displayManager.startx.enable = true;
        xkb.layout = "it";
        windowManager.dwm.enable = true;
    };
}
