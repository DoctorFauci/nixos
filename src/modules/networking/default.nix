{ ... }: {
    networking = {
        hostName = "foo";
        networkmanager.enable = true;
        firewall.allowedTCPPorts = [
            7070
            4444 
        ];
    };
}
