{ inputs, pkgs, ... }: {
    time.timeZone = "Europe/Rome";

    i18n.defaultLocale = "en_US.UTF-8";

    users.users.anon = {
        isNormalUser = true;
        extraGroups = [ "wheel" "audio" "adbusers" ];
    };

    home-manager = {
        extraSpecialArgs = { inherit inputs; };
        users = { "anon" = import ../../home-manager/home.nix; };
    };
}
