{ ... }: {
    imports = [
        ./hardware/default.nix
        ./networking/default.nix
        ./services/default.nix
        ./users/default.nix
    ];
}
