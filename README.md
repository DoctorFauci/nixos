# Personal NixOS configuration
My personal NixOS configuration.

## Table of Contents
- [Tree](#tree)
- [Installation](#installation)
- [Usage](#usage)
- [External links](#externallinks)
- [Contributing](#contributing)
- [License](#license)

## Tree
```
├── LICENSE
├── README.md
└── src
    ├── flake.lock
    ├── flake.nix
    ├── home-manager
    │   └── home.nix
    ├── modules
    │   ├── default.nix
    │   ├── hardware
    │   │   ├── default.nix
    │   │   └── laptop.nix
    │   ├── networking
    │   │   └── default.nix
    │   ├── services
    │   │   ├── default.nix
    │   │   ├── i2pd.nix
    │   │   ├── pipewire.nix
    │   │   ├── tor.nix
    │   │   └── xserver.nix
    │   └── users
    │       └── default.nix
    ├── nixos
    │   └── configuration.nix
    ├── overlays
    │   └── default.nix
    └── pkgs
        └── default.nix

```

## Installation
1. Clone the repository:
```
git clone https://gitlab.com/DoctorFauci/nixos.git
```

2. Rebuild the system:
```
cd nixos/src/
sudo nixos-rebuild switch --flake ./#nixos
```

## Usage
The system uses Xorg, dwm as the tiling window manager and st as the terminal.
It is suggested to have a look at the manpages for dwm and st keybindings.

## External links
Doctor Fauci's [dwm](https://gitlab.com/DoctorFauci/dwm)
and [st](https://gitlab.com/DoctorFauci/st)

## Contributing
1. Fork the repository.
2. Create a new branch: `git checkout -b feature-name`.
3. Make your changes.
4. Push your branch: `git push origin feature-name`.
5. Create a pull request.

## License
This project is licensed under the [MIT License](LICENSE).
